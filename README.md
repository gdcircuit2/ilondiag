# iLonDiag

![screenshot](http://echeloneast.com/images/screenshot.png)

This application will allow you to test devices managed by a smartserver
## Features

* Connect to smartserver
* Fetch device statistics
* Clear device statistics

