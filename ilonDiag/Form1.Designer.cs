﻿namespace ilonDiag
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Grid = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NodeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.App = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Template = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoadAp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Xmit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResetCause = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ErrorLog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLSignal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLFreq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NV1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.But_Exit = new System.Windows.Forms.Button();
            this.butt_SelectAll = new System.Windows.Forms.Button();
            this.butt_SelectNone = new System.Windows.Forms.Button();
            this.button_Test = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.butt_Clear = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.DevicesOnline = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.DevicesOffline = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_PLData = new System.Windows.Forms.Button();
            this.txtIPAdd = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.butGetNV = new System.Windows.Forms.Button();
            this.NVName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Export = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grid
            // 
            this.Grid.AllowUserToAddRows = false;
            this.Grid.AllowUserToDeleteRows = false;
            this.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.NodeName,
            this.NID,
            this.Status,
            this.Comm,
            this.App,
            this.Template,
            this.LoadAp,
            this.PID,
            this.Reset,
            this.Xmit,
            this.ResetCause,
            this.ErrorLog,
            this.PLSignal,
            this.PLMargin,
            this.PLFreq,
            this.NV1});
            this.Grid.Location = new System.Drawing.Point(48, 84);
            this.Grid.Margin = new System.Windows.Forms.Padding(4);
            this.Grid.Name = "Grid";
            this.Grid.RowTemplate.Height = 24;
            this.Grid.Size = new System.Drawing.Size(1208, 607);
            this.Grid.TabIndex = 0;
            this.Grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellContentClick);
            // 
            // Selected
            // 
            this.Selected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Selected.HeaderText = "Select";
            this.Selected.Name = "Selected";
            this.Selected.Width = 53;
            // 
            // NodeName
            // 
            this.NodeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NodeName.HeaderText = "Node";
            this.NodeName.Name = "NodeName";
            this.NodeName.Width = 67;
            // 
            // NID
            // 
            this.NID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NID.HeaderText = "NID";
            this.NID.Name = "NID";
            this.NID.Width = 56;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 73;
            // 
            // Comm
            // 
            this.Comm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Comm.HeaderText = "C";
            this.Comm.MinimumWidth = 22;
            this.Comm.Name = "Comm";
            this.Comm.Width = 22;
            // 
            // App
            // 
            this.App.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.App.HeaderText = "A";
            this.App.MinimumWidth = 22;
            this.App.Name = "App";
            this.App.Width = 22;
            // 
            // Template
            // 
            this.Template.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Template.HeaderText = "T";
            this.Template.MinimumWidth = 22;
            this.Template.Name = "Template";
            this.Template.Width = 22;
            // 
            // LoadAp
            // 
            this.LoadAp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.LoadAp.HeaderText = "L";
            this.LoadAp.MinimumWidth = 22;
            this.LoadAp.Name = "LoadAp";
            this.LoadAp.Width = 22;
            // 
            // PID
            // 
            this.PID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.PID.HeaderText = "P";
            this.PID.MinimumWidth = 22;
            this.PID.Name = "PID";
            this.PID.Width = 22;
            // 
            // Reset
            // 
            this.Reset.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Reset.HeaderText = "R";
            this.Reset.MinimumWidth = 22;
            this.Reset.Name = "Reset";
            this.Reset.Width = 22;
            // 
            // Xmit
            // 
            this.Xmit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Xmit.HeaderText = "Errors";
            this.Xmit.Name = "Xmit";
            this.Xmit.Width = 72;
            // 
            // ResetCause
            // 
            this.ResetCause.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ResetCause.HeaderText = "Last Reset";
            this.ResetCause.Name = "ResetCause";
            this.ResetCause.Width = 101;
            // 
            // ErrorLog
            // 
            this.ErrorLog.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ErrorLog.HeaderText = "Error Log";
            this.ErrorLog.Name = "ErrorLog";
            this.ErrorLog.Width = 93;
            // 
            // PLSignal
            // 
            this.PLSignal.HeaderText = "Signal";
            this.PLSignal.Name = "PLSignal";
            this.PLSignal.Width = 60;
            // 
            // PLMargin
            // 
            this.PLMargin.HeaderText = "Margin";
            this.PLMargin.Name = "PLMargin";
            this.PLMargin.Width = 60;
            // 
            // PLFreq
            // 
            this.PLFreq.HeaderText = "Frequency";
            this.PLFreq.Name = "PLFreq";
            // 
            // NV1
            // 
            this.NV1.HeaderText = "NVData";
            this.NV1.Name = "NV1";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(7, 22);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(69, 27);
            this.button3.TabIndex = 2;
            this.button3.Text = "Fill";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(48, 698);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(195, 105);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "Legend:\r\nC- Commission Request\r\nA-Application Status Change\r\nT-Load Template\r\nL-L" +
                "oad Application\r\nP-Fetch PID\r\nR-Reset Request";
            // 
            // But_Exit
            // 
            this.But_Exit.Location = new System.Drawing.Point(601, 729);
            this.But_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.But_Exit.Name = "But_Exit";
            this.But_Exit.Size = new System.Drawing.Size(95, 31);
            this.But_Exit.TabIndex = 4;
            this.But_Exit.Text = "Exit";
            this.But_Exit.UseVisualStyleBackColor = true;
            this.But_Exit.Click += new System.EventHandler(this.But_Exit_Click);
            // 
            // butt_SelectAll
            // 
            this.butt_SelectAll.Location = new System.Drawing.Point(21, 22);
            this.butt_SelectAll.Margin = new System.Windows.Forms.Padding(4);
            this.butt_SelectAll.Name = "butt_SelectAll";
            this.butt_SelectAll.Size = new System.Drawing.Size(62, 27);
            this.butt_SelectAll.TabIndex = 5;
            this.butt_SelectAll.Text = "All";
            this.butt_SelectAll.UseVisualStyleBackColor = true;
            this.butt_SelectAll.Click += new System.EventHandler(this.butt_SelectAll_Click);
            // 
            // butt_SelectNone
            // 
            this.butt_SelectNone.Location = new System.Drawing.Point(91, 22);
            this.butt_SelectNone.Margin = new System.Windows.Forms.Padding(4);
            this.butt_SelectNone.Name = "butt_SelectNone";
            this.butt_SelectNone.Size = new System.Drawing.Size(59, 27);
            this.butt_SelectNone.TabIndex = 5;
            this.butt_SelectNone.Text = "None";
            this.butt_SelectNone.UseVisualStyleBackColor = true;
            this.butt_SelectNone.Click += new System.EventHandler(this.butt_SelectNone_Click);
            // 
            // button_Test
            // 
            this.button_Test.Location = new System.Drawing.Point(886, 17);
            this.button_Test.Margin = new System.Windows.Forms.Padding(4);
            this.button_Test.Name = "button_Test";
            this.button_Test.Size = new System.Drawing.Size(109, 27);
            this.button_Test.TabIndex = 6;
            this.button_Test.Text = "Query Status";
            this.button_Test.UseVisualStyleBackColor = true;
            this.button_Test.Click += new System.EventHandler(this.button_Test_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 809);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(1301, 25);
            this.statusStrip.TabIndex = 7;
            this.statusStrip.Text = "statusStrip1";
            // 
            // StatusBar
            // 
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(50, 20);
            this.StatusBar.Text = "Ready";
            // 
            // butt_Clear
            // 
            this.butt_Clear.Location = new System.Drawing.Point(1003, 17);
            this.butt_Clear.Margin = new System.Windows.Forms.Padding(4);
            this.butt_Clear.Name = "butt_Clear";
            this.butt_Clear.Size = new System.Drawing.Size(100, 27);
            this.butt_Clear.TabIndex = 8;
            this.butt_Clear.Text = "Clear Status";
            this.butt_Clear.UseVisualStyleBackColor = true;
            this.butt_Clear.Click += new System.EventHandler(this.butt_Clear_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(267, 698);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(195, 105);
            this.textBox2.TabIndex = 9;
            this.textBox2.Text = "Legend:\r\nR-Requested\r\nD-Done\r\nP-Pending\r\nC-Canceled\r\nF-Failed\r\nI-Invoke";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1185, 768);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ver 8.0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(84, 22);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 27);
            this.button1.TabIndex = 11;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DevicesOnline
            // 
            this.DevicesOnline.Location = new System.Drawing.Point(1057, 713);
            this.DevicesOnline.Margin = new System.Windows.Forms.Padding(4);
            this.DevicesOnline.Name = "DevicesOnline";
            this.DevicesOnline.Size = new System.Drawing.Size(56, 22);
            this.DevicesOnline.TabIndex = 12;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(1000, 716);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(49, 17);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "Online";
            // 
            // DevicesOffline
            // 
            this.DevicesOffline.Location = new System.Drawing.Point(1057, 745);
            this.DevicesOffline.Margin = new System.Windows.Forms.Padding(4);
            this.DevicesOffline.Name = "DevicesOffline";
            this.DevicesOffline.Size = new System.Drawing.Size(56, 22);
            this.DevicesOffline.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1000, 748);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Offline";
            // 
            // button_PLData
            // 
            this.button_PLData.Location = new System.Drawing.Point(1149, 17);
            this.button_PLData.Name = "button_PLData";
            this.button_PLData.Size = new System.Drawing.Size(107, 27);
            this.button_PLData.TabIndex = 14;
            this.button_PLData.Text = "Get PL Data";
            this.button_PLData.UseVisualStyleBackColor = true;
            this.button_PLData.Click += new System.EventHandler(this.button_PLData_Click);
            // 
            // txtIPAdd
            // 
            this.txtIPAdd.Location = new System.Drawing.Point(74, 19);
            this.txtIPAdd.Name = "txtIPAdd";
            this.txtIPAdd.Size = new System.Drawing.Size(240, 22);
            this.txtIPAdd.TabIndex = 15;
            this.txtIPAdd.Text = "192.168.1.222";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 19);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 27);
            this.button2.TabIndex = 16;
            this.button2.Text = "Set IP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // butGetNV
            // 
            this.butGetNV.Location = new System.Drawing.Point(886, 49);
            this.butGetNV.Margin = new System.Windows.Forms.Padding(4);
            this.butGetNV.Name = "butGetNV";
            this.butGetNV.Size = new System.Drawing.Size(109, 27);
            this.butGetNV.TabIndex = 6;
            this.butGetNV.Text = "Get NV";
            this.butGetNV.UseVisualStyleBackColor = true;
            this.butGetNV.Click += new System.EventHandler(this.button_NV_Click);
            // 
            // NVName
            // 
            this.NVName.Location = new System.Drawing.Point(1004, 51);
            this.NVName.Name = "NVName";
            this.NVName.Size = new System.Drawing.Size(252, 22);
            this.NVName.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butt_SelectAll);
            this.groupBox1.Controls.Add(this.butt_SelectNone);
            this.groupBox1.Location = new System.Drawing.Point(48, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(168, 61);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Export);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(222, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 61);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Grid";
            // 
            // Export
            // 
            this.Export.Location = new System.Drawing.Point(170, 22);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(82, 27);
            this.Export.TabIndex = 21;
            this.Export.Text = "Export";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.txtIPAdd);
            this.groupBox3.Location = new System.Drawing.Point(486, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(330, 61);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "IP Address";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 834);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.NVName);
            this.Controls.Add(this.button_PLData);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.DevicesOffline);
            this.Controls.Add(this.DevicesOnline);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.butt_Clear);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.butGetNV);
            this.Controls.Add(this.button_Test);
            this.Controls.Add(this.But_Exit);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Grid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "iLonDiag";
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Grid;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button But_Exit;
        private System.Windows.Forms.Button butt_SelectAll;
        private System.Windows.Forms.Button butt_SelectNone;
        private System.Windows.Forms.Button button_Test;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusBar;
        private System.Windows.Forms.Button butt_Clear;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox DevicesOnline;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.TextBox DevicesOffline;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_PLData;
        private System.Windows.Forms.TextBox txtIPAdd;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn NodeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn NID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comm;
        private System.Windows.Forms.DataGridViewTextBoxColumn App;
        private System.Windows.Forms.DataGridViewTextBoxColumn Template;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoadAp;
        private System.Windows.Forms.DataGridViewTextBoxColumn PID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reset;
        private System.Windows.Forms.DataGridViewTextBoxColumn Xmit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResetCause;
        private System.Windows.Forms.DataGridViewTextBoxColumn ErrorLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLSignal;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLFreq;
        private System.Windows.Forms.DataGridViewTextBoxColumn NV1;
        private System.Windows.Forms.Button butGetNV;
        private System.Windows.Forms.TextBox NVName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Export;
    }
}

