﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ilonDiag
{
    class iLON_SoapCalls
    {

        // Enter your SmartServer's Ip Address, e.g. "172.25.137.101"
        public const string _iLonEndpointIpAddress = "zzz.zzz.zzz.zzz";
        static public iLON.iLON100portTypeClient iLON_Client = null;
        /// <summary>
        /// Instantiate the SmartServer Web service for .NET 3.0 and 3.5 (NOT 2.0)
        /// </summary>
        static public void BindClientToSmartServer(string iLonEndpointIpAddress)
        {
            if (iLonEndpointIpAddress.Contains("z"))
            {
                System.Console.Write("FAULT: please set the correct i.LON SmartServer ip-address !!!\n");
                return;
            }
            // Specify the binding to be used for the client.
            System.ServiceModel.Channels.Binding binding
            = new System.ServiceModel.BasicHttpBinding();
            // Initialize the namespace
            binding.Namespace = "http://wsdl.echelon.com/web_services_ns/ilon100/v4.0/message/";
            // The size of the messages that can be received on the wire by services using the
            // BasicHttpBinding is bounded by the amount of memory allocated for each message.
            // This bound on message size is intended to limit exposure to DoS-style attacks.
            // We have to increase it for the SmartServer to be able to receive all messages
            ((System.ServiceModel.BasicHttpBinding)binding).MaxReceivedMessageSize = 1000 * 1000;
            // Obtain the URL of the Web service on the SmartServer.
            System.ServiceModel.EndpointAddress endpointAddress
            = new System.ServiceModel.EndpointAddress("http://"
            + iLonEndpointIpAddress + "/WSDL/iLON100.wsdl");
            // Instantiate the SmartServer Web service object with this address and binding.
            iLON_Client = new iLON.iLON100portTypeClient(binding, endpointAddress);
        }
        /// <summary>
        /// Close the SmartServer Web service
        /// </summary>
        static public void CloseBindingToSmartServer()
        {
            // Closing the client gracefully
            // closes the connection and cleans up resources
            try
            {
                iLON_Client.Close();
            }
            finally
            {
                iLON_Client = null;
            }

        }
    }
}
