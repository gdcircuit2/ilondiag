﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ilonDiag
{
    public partial class Form1 : Form
    {
        public iLON.iLON100portTypeClient SmartServer;
        Properties.Settings MySettings = new Properties.Settings();

        public string LON_DEVICE_SELECT = "//Item[@xsi:type=\"LON_Device_Cfg\"][UCPTlocal=0]";   // backslashes are needed to escape "
        iLON.Item_CfgColl DeviceCfgColl = new iLON.Item_CfgColl();  //result of Get-list of devices configuration
        iLON.Item_Coll DeviceColl = new iLON.Item_Coll();   //result of list-list of devices
        StreamWriter Report = null;

       
        public Form1()
        {
            InitializeComponent();
            txtIPAdd.Text = MySettings.Ipadd;
            NVName.Text = MySettings.Datapoint;

            iLON_SoapCalls.BindClientToSmartServer(txtIPAdd.Text);    //submit IP address from somewhere
//            iLON_SoapCalls.BindClientToSmartServer("192.168.1.222");    //submit IP address from somewhere
            SmartServer = iLON_SoapCalls.iLON_Client;
            iLON.Item_CfgColl Example_Item_CfgColl = new iLON.Item_CfgColl();   //use "iLON" for all types
        }

 
        private void FetchDeviceCfgCollection()
        {
            //iLON.iLON100portTypeClient SmartServer = iLON_SoapCalls.iLON_Client;  //use "SmartServer" for all methods
            DeviceColl.xSelect = LON_DEVICE_SELECT; //apply the filter on the device collection-may not need to do this in vs2008
            DeviceCfgColl = SmartServer.Get(DeviceColl); //get a collection of each devices configuration
        }

        private void PutDevicesInTree()
        {
            int online = 0;
            int offline = 0;

            for (int i = 0; i < DeviceCfgColl.Item.Length; i++)
            {
               iLON.LON_Device_Cfg CurrentDevice = (iLON.LON_Device_Cfg) DeviceCfgColl.Item[i];    //pull next device off the collection and cast it to a lon device type in order to get all properties
                //int NumCommands=CurrentDevice.Command.Length;

                //iLON.LON_Device_CfgCommand
                if (i > (Grid.RowCount - 1))
                {
                    Grid.Rows.Add();    //need to add a row
                }
                
                Grid.Rows[i].Cells[NodeName.DisplayIndex].Value = CurrentDevice.UCPTname;
                Grid.Rows[i].Cells[NID.DisplayIndex].Value = ByteToString(CurrentDevice.UCPTuniqueId, 6);
                if (CurrentDevice.UCPTitemStatus != null)
                {
                    PaintNodeStatusBox(i, Status.DisplayIndex, CurrentDevice.UCPTitemStatus.Value);
                    Grid.Rows[i].Cells[Selected.DisplayIndex].Value = false;
                    offline++;
                }
                else
                {
                    PaintNodeStatusBox(i, Status.DisplayIndex, "Online");
                    Grid.Rows[i].Cells[Selected.DisplayIndex].Value = true;
                    online++;

                }


                if (CurrentDevice.Command != null)
                {
                    for (int j = 0; j < CurrentDevice.Command.Length; j++)
                    {
                        //pull off commands one by one
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.ChangeCommissionStatus)
                            PaintStatusBox(i, Comm.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value); 
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.ChangeApplicationStatus)
                            PaintStatusBox(i, App.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value);
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.GetTemplate)
                            PaintStatusBox(i, Template.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value);
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.ImageDownload)
                            PaintStatusBox(i, LoadAp.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value);
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.Reset)
                            PaintStatusBox(i, Reset.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value);
                        if (CurrentDevice.Command[j].UCPTcommand == iLON.LON_Device_eCommand.FetchProgId)
                            PaintStatusBox(i, PID.DisplayIndex, CurrentDevice.Command[j].UCPTstatus.Value);
                    }
                }

            }
            DevicesOffline.Text = offline.ToString();
            DevicesOnline.Text = online.ToString();

        }

        private void PaintNodeStatusBox(int row, int column, string status)
        {
            if (status == "IS_OFFLINE")
            {
                Grid.Rows[row].Cells[column].Value = "Offline";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Red;

            }
            else if (status == "IS_ONLINE")
            {
                Grid.Rows[row].Cells[column].Value = "Online";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Green;

            }
            else if (status == "IS_UNCONFIGURED")
            {
                Grid.Rows[row].Cells[column].Value = "UnConfig";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Yellow;

            }
            else if (status == "IS_APP_STOPPED")
            {
                Grid.Rows[row].Cells[column].Value = "AppStopped";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Orange;

            }
            else if (status == "IS_DELETED")
            {
                Grid.Rows[row].Cells[column].Value = "Deleted";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.SkyBlue;

            }
            else if (status == "IS_NOTSYNCED")
            {
                Grid.Rows[row].Cells[column].Value = "NotSynced";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Orange;

            }
            else 
            {
                Grid.Rows[row].Cells[column].Value = "Online";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Green;

            }

        }

        private void PaintStatusBox(int row, int column, string status)
        {
            if (status == "STATUS_DONE")
            {
                Grid.Rows[row].Cells[column].Value = "D";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Green;

            }
            if (status == "STATUS_REQUEST")
            {
                Grid.Rows[row].Cells[column].Value = "R";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Yellow;

            }
            if (status == "STATUS_CANCEL")
            {
                Grid.Rows[row].Cells[column].Value = "C";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.SkyBlue;

            }
            if (status == "STATUS_PENDING")
            {
                Grid.Rows[row].Cells[column].Value = "P";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Yellow;

            }
            if (status == "STATUS_FAIL")
            {
                Grid.Rows[row].Cells[column].Value = "F";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Red;

            }
            if (status == "STATUS_INVOKE")
            {
                Grid.Rows[row].Cells[column].Value = "I";
                Grid.Rows[row].Cells[column].Style.BackColor = Color.Yellow;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            FetchDeviceCfgCollection();
            PutDevicesInTree();
            Cursor = Cursors.Default;
        }

        private string ByteToString(byte[] ByteArray, int NumBytes)
        {
            string StrData = string.Empty;
            for (int i = 0; i < NumBytes; i++)
            {
                StrData += ByteArray[i].ToString("X2");
            }
            return StrData;

        }

        private void But_Exit_Click(object sender, EventArgs e)
        {
            MySettings.Ipadd = txtIPAdd.Text;
            MySettings.Datapoint = NVName.Text;
            MySettings.Save();
            iLON_SoapCalls.CloseBindingToSmartServer();
            Application.Exit();
        }

        private void butt_SelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow R in Grid.Rows)
            {
                R.Cells[Selected.DisplayIndex].Value = true;

            }
        }

        private void butt_SelectNone_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow R in Grid.Rows)
            {
                R.Cells[Selected.DisplayIndex].Value = false;

            }

        }

        private void button_Test_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

           //iLON.iLON100portTypeClient SmartServer = iLON_SoapCalls.iLON_Client;  //use "SmartServer" for all methods
            
            //int count = 0;
            iLON.Item_Coll InvokeDeviceColl = new iLON.Item_Coll();   //single device collection to fetch status
            iLON.Item_Coll ResponseDeviceColl = new iLON.Item_Coll();   //response from invoke
            InvokeDeviceColl.Item = new iLON.LON_Device_Command_Invoke[1];  //add one device to teh collection
            iLON.LON_Device_Command_Invoke InvokeItem = new ilonDiag.iLON.LON_Device_Command_Invoke();

            InvokeItem.Command = iLON.LON_Device_eCommand.QueryStatus;
            
            foreach (DataGridViewRow R in Grid.Rows)
            {
                if ((bool)R.Cells[Selected.DisplayIndex].Value)
                {
                    InvokeItem.UCPTname= R.Cells[NodeName.DisplayIndex].Value.ToString();
                    InvokeDeviceColl.Item[0] = InvokeItem;
                    SmartServer.InvokeCmd(ref InvokeDeviceColl); //get a collection of each devices configuration
                    iLON.LON_Device_StatusData_InvokeResponse ResponseInfo = (iLON.LON_Device_StatusData_InvokeResponse)InvokeDeviceColl.Item[0];
                    if (ResponseInfo.UCPTonlineStatus != null)
                    {

                        R.Cells[Xmit.DisplayIndex].Value = ResponseInfo.UCPTtransmitErrors.ToString();

                        if (ResponseInfo.UCPTresetCause != null)
                            R.Cells[ResetCause.DisplayIndex].Value = ResponseInfo.UCPTresetCause.Value;
                        if (ResponseInfo.UCPTerrorLog != null)
                            R.Cells[ErrorLog.DisplayIndex].Value = ResponseInfo.UCPTerrorLog.Value;
                    }
                    else
                    {
                        R.Cells[Xmit.DisplayIndex].Value = "?";
                        R.Cells[ResetCause.DisplayIndex].Value = "?";
                        R.Cells[ErrorLog.DisplayIndex].Value = "?";
                    }

                    //count++;
                    R.Selected = true;
                    Grid.Update();
                    R.Selected = false;
                }
            }
           // StatusBar.Text = count.ToString();
          //StatusBar.Text = ResponseInfo.UCPTversionNumber.ToString();
            Cursor = Cursors.Arrow;
        }

        private void button_PLData_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            //iLON.iLON100portTypeClient SmartServer = iLON_SoapCalls.iLON_Client;  //use "SmartServer" for all methods

            //int count = 0;
            iLON.Item_Coll InvokeDeviceColl = new iLON.Item_Coll();   //single device collection to fetch status
            iLON.Item_Coll ResponseDeviceColl = new iLON.Item_Coll();   //response from invoke
            InvokeDeviceColl.Item = new iLON.LON_Device_Command_Invoke[1];  //add one device to teh collection
            iLON.LON_Device_Command_Invoke InvokeItem = new ilonDiag.iLON.LON_Device_Command_Invoke();
            iLON.LON_Device_DataFrequencyInfo Freq1 = new ilonDiag.iLON.LON_Device_DataFrequencyInfo();

            InvokeItem.Command = iLON.LON_Device_eCommand.PowerlineData;

            foreach (DataGridViewRow R in Grid.Rows)
            {
                if ((bool)R.Cells[Selected.DisplayIndex].Value)
                {
                    InvokeItem.UCPTname = R.Cells[NodeName.DisplayIndex].Value.ToString();
                    InvokeDeviceColl.Item[0] = InvokeItem;
                    SmartServer.InvokeCmd(ref InvokeDeviceColl); //get a collection of each devices configuration
                    iLON.LON_Device_PowerlineData_InvokeResponse ResponseInfo = (iLON.LON_Device_PowerlineData_InvokeResponse)InvokeDeviceColl.Item[0];
                    //iLON.LON_Device_StatusData_InvokeResponse ResponseInfo = (iLON.LON_Device_StatusData_InvokeResponse)InvokeDeviceColl.Item[0];
                    if (ResponseInfo.FrequencyInfo != null)
                    {
                        Freq1 = ResponseInfo.FrequencyInfo[1];
                        R.Cells[PLSignal.DisplayIndex].Value = Freq1.UCPTsignalStrength.ToString() + "db";
                        R.Cells[PLMargin.DisplayIndex].Value = Freq1.UCPTsignalMargin.ToString() + "db";

                        R.Cells[PLFreq.DisplayIndex].Value = Freq1.UCPTfrequency.ToString();
                        if (Freq1.UCPTfrequency == 0)
                            R.Cells[PLFreq.DisplayIndex].Value = "Primary";

                        if (Freq1.UCPTfrequency == 1)
                            R.Cells[PLFreq.DisplayIndex].Value = "Secondary";
                    }
                   //if (ResponseInfo.UCPTresetCause != null)
                    //    R.Cells[ResetCause.DisplayIndex].Value = ResponseInfo.UCPTresetCause.Value;
                    //if (ResponseInfo.UCPTerrorLog != null)
                    //    R.Cells[ErrorLog.DisplayIndex].Value = ResponseInfo.UCPTerrorLog.Value;
                    //count++;
                    R.Selected = true;
                    Grid.Update();
                    R.Selected = false;
                }
            }
            // StatusBar.Text = count.ToString();
            //StatusBar.Text = ResponseInfo.UCPTversionNumber.ToString();
            Cursor = Cursors.Arrow;
        }


        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Selected.DisplayIndex)
            {
               // R.Cells[SelectDevice.DisplayIndex].Value = true;
                DataGridViewRow R = Grid.Rows[e.RowIndex];

                if ((bool)R.Cells[Selected.DisplayIndex].Value == true)
                {
                    R.Cells[Selected.DisplayIndex].Value = false;
                }
                if ((bool)R.Cells[Selected.DisplayIndex].Value == false)
                {
                    R.Cells[Selected.DisplayIndex].Value = true;
                }
            }

        }

        private void butt_Clear_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            iLON.iLON100portTypeClient SmartServer = iLON_SoapCalls.iLON_Client;  //use "SmartServer" for all methods

            iLON.Item_Coll InvokeDeviceColl = new iLON.Item_Coll();   //single device collection to fetch status
            iLON.LON_Device_Command_Invoke InvokeItem = new ilonDiag.iLON.LON_Device_Command_Invoke();

            InvokeItem.Command = iLON.LON_Device_eCommand.ClearStatus;

            foreach (DataGridViewRow R in Grid.Rows)
            {
                if ((bool)R.Cells[Selected.DisplayIndex].Value)
                {
                    InvokeItem.UCPTname = R.Cells[NodeName.DisplayIndex].Value.ToString();
                    InvokeDeviceColl.Item = new iLON.LON_Device_Command_Invoke[1];  //add one device to teh collection
                    InvokeDeviceColl.Item[0] = InvokeItem;
                    SmartServer.InvokeCmd(ref InvokeDeviceColl); //get a collection of each devices configuration
                    //iLON.LON_Device_StatusData_InvokeResponse ResponseInfo = (iLON.LON_Device_StatusData_InvokeResponse)InvokeDeviceColl.Item[0];
                   // R.Cells[Xmit.DisplayIndex].Value = ResponseInfo.UCPTtransmitErrors.ToString();
                    //count++;
                    R.Selected = true;
                    Grid.Update();
                    R.Selected = false;
                }
            }
            // StatusBar.Text = count.ToString();
            //StatusBar.Text = ResponseInfo.UCPTversionNumber.ToString();
            Cursor = Cursors.Arrow;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Grid.Rows.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MySettings.Ipadd = txtIPAdd.Text;
            iLON_SoapCalls.CloseBindingToSmartServer();
            iLON_SoapCalls.BindClientToSmartServer(txtIPAdd.Text);    //submit IP address from somewhere
            SmartServer = iLON_SoapCalls.iLON_Client;

        }

        private void button_NV_Click(object sender, EventArgs e)
        {
            string newValue;
            string oldValue=null;

            Cursor = Cursors.WaitCursor;

            //iLON.iLON100portTypeClient SmartServer = iLON_SoapCalls.iLON_Client;  //use "SmartServer" for all methods

            //int count = 0;
            iLON.Item_Coll ReadColl = new iLON.Item_Coll();   //single device collection to fetch dp value
            iLON.Item_DataColl ResponseColl = new iLON.Item_DataColl();   //response from read
            ReadColl.Item = new iLON.Item[1];  //add one dp to teh collection
            //iLON.LON_Device_Command_Invoke InvokeItem = new ilonDiag.iLON.LON_Device_Command_Invoke();
            ReadColl.Item[0] = new iLON.Dp_Data();
           // InvokeItem.Command = iLON.LON_Device_eCommand.QueryStatus;

            foreach (DataGridViewRow R in Grid.Rows)
            {
                if ((bool)R.Cells[Selected.DisplayIndex].Value)
                {
                    // set the DP name
                    ReadColl.Item[0].UCPTname = R.Cells[NodeName.DisplayIndex].Value.ToString() + NVName.Text;
                    
                    //fix from here
                    //InvokeItem.UCPTname = R.Cells[NodeName.DisplayIndex].Value.ToString();
                   // InvokeDeviceColl.Item[0] = InvokeItem;
                    //SmartServer.InvokeCmd(ref InvokeDeviceColl); //get a collection of each devices configuration

                    ((iLON.Dp_Data)(ReadColl.Item[0])).UCPTmaxAge = 0;
                    ((iLON.Dp_Data)(ReadColl.Item[0])).UCPTmaxAgeSpecified = true;
                    //call the Read Function
                    ResponseColl = SmartServer.Read(ReadColl);

                    
                   // iLON.LON_Device_StatusData_InvokeResponse ResponseInfo = (iLON.LON_Device_StatusData_InvokeResponse)InvokeDeviceColl.Item[0];
                    if (ResponseColl.Item != null)
                    {
                        if (ResponseColl.Item[0].fault == null)
                        {

                            newValue = ((iLON.Dp_Data)ResponseColl.Item[0]).UCPTvalue[0].Value;
                            if (R.Cells[NV1.DisplayIndex].Value != null)
                                oldValue = R.Cells[NV1.DisplayIndex].Value.ToString();
                            else
                                oldValue = newValue;
                            if (string.Compare(newValue, oldValue) != 0)
                                R.Cells[NV1.DisplayIndex].Style.BackColor = Color.SkyBlue;
                            else
                                R.Cells[NV1.DisplayIndex].Style.BackColor = Color.White;

                            //R.Cells[NV1.DisplayIndex].Value = ((iLON.Dp_Data)ResponseColl.Item[0]).UCPTvalue[0].Value;
                            R.Cells[NV1.DisplayIndex].Value = newValue;
                        }
                        else
                        {
                            R.Cells[NV1.DisplayIndex].Value = "?";
                        }

                    }
                    else
                    {
                        R.Cells[NV1.DisplayIndex].Value = "?";
                       // R.Cells[ResetCause.DisplayIndex].Value = "?";
                        //R.Cells[ErrorLog.DisplayIndex].Value = "?";
                    }

                    //count++;
                    R.Selected = true;
                    Grid.Update();
                    R.Selected = false;
                }
            }
            // StatusBar.Text = count.ToString();
            //StatusBar.Text = ResponseInfo.UCPTversionNumber.ToString();
            Cursor = Cursors.Arrow;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string CurrentLine=null;
            CurrentLine="\"Selected\",\"Node\",\"Nid\",\"Status\",\"Commission\",\"App Status\",\"Template\",\"Application\",\"ProgramID\",\"Reset\",\"CRC Errors\",\"Last Reset\",\"Error Log\",\"Signal Strength\",\"PL Margin\",\"Frequency\",\"NV Data\"";
            int i;

            Report = new StreamWriter("TestReport.csv");
            Report.WriteLine(CurrentLine);
            CurrentLine = null;
            foreach (DataGridViewRow R in Grid.Rows)
            {
                for (i = 0; i < R.Cells.Count; i++)
                {
                    if(i==0)
                        CurrentLine += "\"";
                    else
                        CurrentLine += ",\"";

                    if (R.Cells[i].Value != null)
                    {
                        CurrentLine += R.Cells[i].Value.ToString();
                        CurrentLine += "\"";
                    }
                    else
                        CurrentLine += "\"";
                }
                Report.WriteLine(CurrentLine);
                CurrentLine = null;
            }
            Report.Close();
        }

    }
}
